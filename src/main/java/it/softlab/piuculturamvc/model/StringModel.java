package it.softlab.piuculturamvc.model;

import java.io.Serializable;

public class StringModel implements Serializable {
    String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public StringModel() {
    }

    public StringModel(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "StringModel{" +
                "value='" + value + '\'' +
                '}';
    }
}
