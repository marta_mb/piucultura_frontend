package it.softlab.piuculturamvc.model.request;


import it.softlab.piuculturamvc.model.ResponseModel;
import it.softlab.piuculturamvc.model.general.UserSummaryDto;

import java.util.Date;

public class ResponseUserSummary extends ResponseModel {

    private UserSummaryDto result;

    public ResponseUserSummary() {
    }

    public ResponseUserSummary(UserSummaryDto result) {
        this.result = result;
    }

    public ResponseUserSummary(String errorCode, String errorMessage, UserSummaryDto result) {
        super(errorCode, errorMessage);
        this.result = result;
    }

    public ResponseUserSummary(Date timestamp, String errorCode, String errorMessage, UserSummaryDto result) {
        super(timestamp, errorCode, errorMessage);
        this.result = result;
    }

    public UserSummaryDto getResult() {
        return result;
    }

    public void setResult(UserSummaryDto
                                  result) {
        this.result = result;
    }
}
