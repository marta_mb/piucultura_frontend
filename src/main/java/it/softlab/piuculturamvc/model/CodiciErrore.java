package it.softlab.piuculturamvc.model;

public class CodiciErrore {
    /**
     * CODICE ERRORE GENERICI
     */
    public final static String GENERIC_EXC = "GEN_000";
    public final static String NOT_FOUND = "GEN_001";
    public final static String BAD_REQUEST = "GEN_002";
    public final static String HTTP_ERROR_NOT_ACCEPTABLE = "GEN_003";
    public final static String SQL_ERROR = "GEN_004";


    /**
     * CODICE ERRORE ACCESSO
     */
    public final static String AUTH_UNAUTHORIZED = "AUTH_001";
    public final static String AUTH_TOKEN_EXPIRED = "AUTH_002";
    public final static String AUTH_UNMATCH_ISSURER = "AUTH_003";
    public final static String AUTH_USER_HAS_NOT_PERMISSION = "AUTH_004";
    public final static String AUTH_USER_HAS_NOT_NOT_GROUPS = "AUTH_005";


    /**
     * CODICE ERRORE CAMBIO PASSWORD
     */
    public final static String CHPS_USERNAME_ERROR = "CHPS_001";
    public final static String CHPS_OLD_PSW_ERROR = "CHPS_002";
    public final static String CHPS_NEW_PSW_ERROR = "CHPS_003";

    /**
     * CODICE ERRORE REGISTRAZIONE
     */

    public final static String REG_USERNAME_ERROR = "REG_001";
    public final static String REG_EMAIL_ERROR = "REG_002";
    public final static String REG_GROUP_ERROR = "REG_003";




}
