package it.softlab.piuculturamvc.model;


import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;


public class ResponseModel {
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy@hh:mm",  timezone = "CET")
	private Date timestamp;
	private Boolean isError;
	private String errorCode;
	private String errorMessage;



	public ResponseModel() {
		this.timestamp = new Date();
		this.isError = false;
	}

	public ResponseModel(String errorCode, String errorMessage) {
		this.timestamp = new Date();
		this.isError = true;
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

	public ResponseModel(Date timestamp, String errorCode, String errorMessage) {
		this.timestamp = timestamp;
		this.isError = true;
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public Boolean getError() {
		return isError;
	}

	public void setError(Boolean error) {
		isError = error;
	}

}
