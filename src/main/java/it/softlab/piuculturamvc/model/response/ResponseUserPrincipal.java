package it.softlab.piuculturamvc.model.response;

import it.softlab.piuculturamvc.model.ResponseModel;
import it.softlab.piuculturamvc.model.general.UserPrincipalDto;

import java.io.Serializable;
import java.util.Date;

public class ResponseUserPrincipal extends ResponseModel implements Serializable {

    private UserPrincipalDto result;

    public ResponseUserPrincipal() {
    }

    public ResponseUserPrincipal(UserPrincipalDto result) {
        this.result = result;
    }

    public ResponseUserPrincipal(String errorCode, String errorMessage, UserPrincipalDto result) {
        super(errorCode, errorMessage);
        this.result = result;
    }

    public ResponseUserPrincipal(Date timestamp, String errorCode, String errorMessage, UserPrincipalDto result) {
        super(timestamp, errorCode, errorMessage);
        this.result = result;
    }

    public UserPrincipalDto getResult() {
        return result;
    }

    public void setResult(UserPrincipalDto result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "ResponseUserPrincipal{" +
                "result=" + result +
                '}';
    }
}
