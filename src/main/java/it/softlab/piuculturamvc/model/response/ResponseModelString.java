package it.softlab.piuculturamvc.model.response;


import it.softlab.piuculturamvc.model.ResponseModel;

import java.util.Date;


public class ResponseModelString extends ResponseModel {
	String result;

	public String getResult() {
		return result;
	}

	public ResponseModelString() {
	}

	public void setResult(String result) {
		this.result = result;
	}

	public ResponseModelString(String result) {
		this.result = result;
	}

	public ResponseModelString(String errorCode, String errorMessage, String result) {
		super(errorCode, errorMessage);
		this.result = result;
	}

	public ResponseModelString(Date timestamp, String errorCode, String errorMessage, String result) {
		super(timestamp, errorCode, errorMessage);
		this.result = result;
	}

	@Override
	public String toString() {
		return "ResponseModelString{" +
				"result='" + result + '\'' +
				'}';
	}
}
