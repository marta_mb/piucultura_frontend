package it.softlab.piuculturamvc.model.response;

import it.softlab.piuculturamvc.model.ResponseModel;
import it.softlab.piuculturamvc.model.general.UserPrincipalDto;
import it.softlab.piuculturamvc.model.general.UserPrincipalDtoSmalVersion;

import java.io.Serializable;
import java.util.Date;

public class ResponseUserPrincipalSmallVersion extends ResponseModel implements Serializable {

    private UserPrincipalDtoSmalVersion result;

    public ResponseUserPrincipalSmallVersion() {
    }

    public ResponseUserPrincipalSmallVersion(UserPrincipalDtoSmalVersion result) {
        this.result = result;
    }

    public ResponseUserPrincipalSmallVersion(String errorCode, String errorMessage, UserPrincipalDtoSmalVersion result) {
        super(errorCode, errorMessage);
        this.result = result;
    }

    public ResponseUserPrincipalSmallVersion(Date timestamp, String errorCode, String errorMessage, UserPrincipalDtoSmalVersion result) {
        super(timestamp, errorCode, errorMessage);
        this.result = result;
    }

    public UserPrincipalDtoSmalVersion getResult() {
        return result;
    }

    public void setResult(UserPrincipalDtoSmalVersion result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "ResponseUserPrincipal{" +
                "result=" + result +
                '}';
    }
}
