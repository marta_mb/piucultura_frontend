package it.softlab.piuculturamvc.model.response;

import it.softlab.piuculturamvc.model.ResponseModel;

import java.util.Date;

public class ResponseCambioPassword extends ResponseModel {
    private String result;

    public ResponseCambioPassword() {
    }

    public ResponseCambioPassword(String result) {
        this.result = result;
    }

    public ResponseCambioPassword(String errorCode, String errorMessage, String result) {
        super(errorCode, errorMessage);
        this.result = result;
    }

    public ResponseCambioPassword(Date timestamp, String errorCode, String errorMessage, String result) {
        super(timestamp, errorCode, errorMessage);
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
