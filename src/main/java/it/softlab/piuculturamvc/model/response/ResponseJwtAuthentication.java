package it.softlab.piuculturamvc.model.response;

import it.softlab.piuculturamvc.model.ResponseModel;
import it.softlab.piuculturamvc.model.general.JwtAuthenticationDTO;

import java.io.Serializable;
import java.util.Date;

public class ResponseJwtAuthentication extends ResponseModel implements Serializable {

    private JwtAuthenticationDTO result;

    public ResponseJwtAuthentication() {
    }

    public ResponseJwtAuthentication(JwtAuthenticationDTO result) {
        this.result = result;
    }

    public ResponseJwtAuthentication(String errorCode, String errorMessage, JwtAuthenticationDTO result) {
        super(errorCode, errorMessage);
        this.result = result;
    }

    public ResponseJwtAuthentication(Date timestamp, String errorCode, String errorMessage, JwtAuthenticationDTO result) {
        super(timestamp, errorCode, errorMessage);
        this.result = result;
    }

    public JwtAuthenticationDTO getResult() {
        return result;
    }

    public void setResult(JwtAuthenticationDTO result) {
        this.result = result;
    }


    @Override
    public String toString() {
        return "ResponseJwtAuthentication{" +
                "result=" + result +
                '}';
    }
}
