package it.softlab.piuculturamvc.model.general;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Objects;

/***
 * La nostra classe custom che implementa lo UserDetails. Questa è la classe la cui istanza verrà restituita
 * dalla nostra UserDetailsService personalizzata. Spring Security userà le informazioni registrate nell'oggetto
 * UserPrincipalDto per effettuare operazioni di autorizzazione e autenticazione.
 */
public class UserPrincipalDtoSmalVersion  {


    private String name;

    private String username;

    @JsonIgnore
    private String email;

    public UserPrincipalDtoSmalVersion() {
    }

    public UserPrincipalDtoSmalVersion( String name, String username, String email) {
        this.name = name;
        this.username = username;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }



    @Override
    public String toString() {
        return "UserPrincipalDto{" +
                ", name='" + name + '\'' +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
