package it.softlab.piuculturamvc.model.general;

import com.fasterxml.jackson.annotation.JsonIgnore;
import it.softlab.piuculturamvc.entity.Disabilita;
import it.softlab.piuculturamvc.entity.Interessi;
import it.softlab.piuculturamvc.entity.UserPrincipal;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.*;
import java.util.stream.Collectors;


/***
 * La nostra classe custom che implementa lo UserDetails. Questa è la classe la cui istanza verrà restituita
 * dalla nostra UserDetailsService personalizzata. Spring Security userà le informazioni registrate nell'oggetto
 * UserPrincipalDto per effettuare operazioni di autorizzazione e autenticazione.
 */

/*
ACCOUNT è CONSIDERATO ABILITATO DOPO CHE L'EMAIL è STATA CONSEGANTA
 */
public class UserPrincipalDto  {

    private Long id;

    private String name;

    private String surname;

    private String username;

    private String email;

    private String password;

    private Date birth;

    private String country;

    private String profession;

    private Collection<Disabilita> disability;

    private Collection<Interessi> interests;

    private String resetToken;

    private String confirmToken;

    private Boolean confirmEmail;

    private String qualification;

/*
    private Collection<Role> authorities;
*/


    public UserPrincipalDto() {
        super();
    }

    public UserPrincipalDto(Long id, String name, String surname, String username, String email, String password, Date birth, String country, String profession, Collection<Disabilita> disability, Collection<Interessi> interests, String resetToken, String confirmToken, Boolean confirmEmail, String qualification, Collection<? extends GrantedAuthority> authorities) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.email = email;
        this.password = password;
        this.birth = birth;
        this.country = country;
        this.profession = profession;
        this.disability = disability;
        this.interests = interests;
        this.resetToken = resetToken;
        this.confirmToken = confirmToken;
        this.confirmEmail = confirmEmail;
        this.qualification = qualification;
        //this.authorities = authorities;
    }

    public static UserPrincipalDto create(UserPrincipal user) {

        List<GrantedAuthority> authorities = user.getGroups().stream().map(role ->
                new SimpleGrantedAuthority(role.getName().name())
        ).collect(Collectors.toList());

        List<Interessi>  interests = user.getInterests().stream().map(interessi ->
                new Interessi(interessi.getName(), interessi.getAltriInteressi())
        ).collect(Collectors.toList());

        List<Disabilita>  disability = user.getDisability().stream().map(disabilita ->
                new Disabilita(disabilita.getName(), disabilita.getAltreDisalitita())
        ).collect(Collectors.toList());

        return new UserPrincipalDto(
                user.getId(),
                user.getName(),
                user.getSurname(),
                user.getUsername(),
                user.getEmail(),
                user.getPassword(),
                user.getBirth(),
                user.getCountry(),
                user.getProfession(),
                disability,
                interests,
                user.getResetToken(),
                user.getConfirmToken(),
                user.getConfirmEmail(),
                user.getQualification(),
                authorities
        );

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public Collection<Disabilita> getDisability() {
        return disability;
    }

    public void setDisability(Collection<Disabilita> disability) {
        this.disability = disability;
    }

    public Collection<Interessi> getInterests() {
        return interests;
    }

    public void setInterests(Collection<Interessi> interests) {
        this.interests = interests;
    }

    public String getResetToken() {
        return resetToken;
    }

    public void setResetToken(String resetToken) {
        this.resetToken = resetToken;
    }

    public String getConfirmToken() {
        return confirmToken;
    }

    public void setConfirmToken(String confirmToken) {
        this.confirmToken = confirmToken;
    }

    public Boolean getConfirmEmail() {
        return confirmEmail;
    }

    public void setConfirmEmail(Boolean confirmEmail) {
        this.confirmEmail = confirmEmail;
    }

    /*@Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }*/

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

   /* public void setAuthoritiess(Collection<? extends GrantedAuthority> authorities) {
        this.authorities = authorities;
    }
    public Collection<? extends GrantedAuthority> getAuthoritiess() {
        return this.authorities;
    }
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {


        List<GrantedAuthority> authorities =
                new ArrayList<GrantedAuthority>();
        authorities.addAll((Collection<? extends GrantedAuthority>) getAuthoritiess());

        return authorities;
    }


    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }*/

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    @Override
    public String toString() {
        return "UserPrincipalDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", birth=" + birth +
                ", country='" + country + '\'' +
                ", profession='" + profession + '\'' +
                ", disability=" + disability +
                ", interests=" + interests +
                ", resetToken='" + resetToken + '\'' +
                ", confirmToken='" + confirmToken + '\'' +
                ", confirmEmail=" + confirmEmail +
                ", qualification='" + qualification + '\'' +
               // ", authorities=" + authorities +
                '}';
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}