package it.softlab.piuculturamvc.services;

import it.softlab.piuculturamvc.model.StringModel;
import it.softlab.piuculturamvc.model.general.UserPrincipalDto;
import it.softlab.piuculturamvc.model.request.RequestCambioPassword;
import it.softlab.piuculturamvc.model.request.RequestLogin;
import it.softlab.piuculturamvc.model.request.RequestSignUp;
import it.softlab.piuculturamvc.model.request.ResponseUserSummary;
import it.softlab.piuculturamvc.model.response.ResponseUserPrincipal;
import it.softlab.piuculturamvc.model.response.ResponseUserPrincipalSmallVersion;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@FeignClient(name = "user-service", url="${backend.url}"+"/", fallback = UserServiceImpl.class )
/*
@FeignClient(name = "user-service", url = "http://127.0.0.1:8030/userDTO", fallback = UserServiceImpl.class )
*/

public interface UserService {

    @PostMapping("/auth/signin")
    ResponseEntity<?> authenticateUser(@Valid @RequestBody RequestLogin loginReqeust);

    @PostMapping("/user/userinfo/")
    public ResponseEntity<ResponseUserPrincipal> getCurrentUser(@RequestHeader("token") String token, @RequestBody StringModel userToken);//@CurrentUser

    @GetMapping("/user/userinfo/{username}")
    public ResponseEntity<ResponseUserPrincipalSmallVersion> getCurrentUserSmallVersion(@PathVariable(name = "username") String username, @RequestHeader(value = "Authorization", required = true) String authorizationHeader);//@CurrentUser


    @PostMapping("auth/signup")
    public ResponseEntity<ResponseUserPrincipal> registerUser( @RequestBody RequestSignUp requestSignUp);

    @PostMapping("user/updateuser")
    public ResponseEntity<ResponseUserPrincipal> updateUser( @RequestBody RequestSignUp requestSignUp);


    @PostMapping("/cambiapassword")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> resetPassword(@RequestBody RequestCambioPassword requestCambioPassword);

    }
