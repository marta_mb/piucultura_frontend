package it.softlab.piuculturamvc.services;


import it.softlab.piuculturamvc.model.StringModel;
import it.softlab.piuculturamvc.model.general.UserPrincipalDto;
import it.softlab.piuculturamvc.model.request.RequestCambioPassword;
import it.softlab.piuculturamvc.model.request.RequestLogin;
import it.softlab.piuculturamvc.model.request.RequestResetPassword;
import it.softlab.piuculturamvc.model.request.RequestSignUp;
import it.softlab.piuculturamvc.model.response.ResponseCambioPassword;
import it.softlab.piuculturamvc.model.response.ResponseJwtAuthentication;
import it.softlab.piuculturamvc.model.response.ResponseModelString;
import it.softlab.piuculturamvc.model.response.ResponseUserPrincipal;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;


import javax.validation.Valid;

@Component
public class  AuthServiceImpl implements AuthService {


    @Override
    public ResponseEntity<ResponseJwtAuthentication> authenticateUser(@Valid RequestLogin loginReqeust) {
        return null;
    }

    @Override
    public ResponseEntity<ResponseUserPrincipal> registerUser(@Valid RequestSignUp requestSignUp) {
        return null;
    }

    @Override
    public ResponseEntity<ResponseCambioPassword> processForgotPasswordForm(StringModel userEmail) {
        return null;
    }

    @Override
    public ResponseEntity<ResponseModelString> reinviaEmailConvermaAccount(@Valid StringModel email) {
        return null;
    }

    @Override
    public ResponseEntity<ResponseCambioPassword> processResetPasswordForm(@Valid RequestResetPassword requestResetPassword) {
        return null;
    }

    @Override
    public ResponseEntity<ResponseCambioPassword> processSavePasswordForm(@Valid RequestResetPassword requestResetPassword) {
        return null;
    }

    @Override
    public ResponseEntity<ResponseCambioPassword> processSavePsw(@Valid RequestResetPassword requestResetPassword) {
        return null;
    }

    @Override
    public ResponseEntity<ResponseCambioPassword> processForgotPasswordForm(String token, @Valid RequestCambioPassword email) {
        return null;
    }


}
