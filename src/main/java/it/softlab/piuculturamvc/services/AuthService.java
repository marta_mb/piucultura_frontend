package it.softlab.piuculturamvc.services;


import it.softlab.piuculturamvc.model.StringModel;
import it.softlab.piuculturamvc.model.general.UserPrincipalDto;
import it.softlab.piuculturamvc.model.request.RequestCambioPassword;
import it.softlab.piuculturamvc.model.request.RequestLogin;
import it.softlab.piuculturamvc.model.request.RequestResetPassword;
import it.softlab.piuculturamvc.model.request.RequestSignUp;
import it.softlab.piuculturamvc.model.response.ResponseCambioPassword;
import it.softlab.piuculturamvc.model.response.ResponseJwtAuthentication;
import it.softlab.piuculturamvc.model.response.ResponseModelString;
import it.softlab.piuculturamvc.model.response.ResponseUserPrincipal;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

//quando fallisce chiama impl
@FeignClient(name = "auth-service",url="${backend.url}" ) // fallback = AuthServiceImpl.class
public interface AuthService {

    @PostMapping("/auth/signin")
    ResponseEntity<ResponseJwtAuthentication> authenticateUser(@Valid @RequestBody RequestLogin loginReqeust);

    @PostMapping("/auth/signup")
    ResponseEntity<ResponseUserPrincipal> registerUser(@Valid @RequestBody RequestSignUp requestSignUp);

    @RequestMapping(value = "/auth/forgot/", method = RequestMethod.GET)
    ResponseEntity<ResponseCambioPassword>  processForgotPasswordForm(@Valid @RequestBody StringModel email);

    @RequestMapping(value = "/auth/resend/", method = RequestMethod.POST)
    ResponseEntity<ResponseModelString>  reinviaEmailConvermaAccount(@Valid @RequestBody StringModel email);

    @RequestMapping(value = "/auth/reset/", method = RequestMethod.GET)
    ResponseEntity<ResponseCambioPassword>  processResetPasswordForm(@Valid @RequestBody RequestResetPassword requestResetPassword);

    @RequestMapping(value = "/auth/confirm", method = RequestMethod.GET)
    ResponseEntity<ResponseCambioPassword>  processSavePasswordForm(@Valid @RequestBody RequestResetPassword requestResetPassword);


    @RequestMapping(value = "/auth/savepassword", method = RequestMethod.POST)
    ResponseEntity<ResponseCambioPassword>  processSavePsw(@Valid @RequestBody RequestResetPassword requestResetPassword);

    @RequestMapping(value = "/user/changepassword/", method = RequestMethod.POST)
    ResponseEntity<ResponseCambioPassword>  processForgotPasswordForm(@RequestHeader("token") String token, @Valid @RequestBody RequestCambioPassword email);


}
