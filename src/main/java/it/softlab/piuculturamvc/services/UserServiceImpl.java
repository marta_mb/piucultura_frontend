package it.softlab.piuculturamvc.services;

import it.softlab.piuculturamvc.model.StringModel;
import it.softlab.piuculturamvc.model.general.UserPrincipalDto;
import it.softlab.piuculturamvc.model.request.RequestCambioPassword;
import it.softlab.piuculturamvc.model.request.RequestLogin;
import it.softlab.piuculturamvc.model.request.RequestSignUp;
import it.softlab.piuculturamvc.model.request.ResponseUserSummary;
import it.softlab.piuculturamvc.model.response.ResponseUserPrincipal;
import it.softlab.piuculturamvc.model.response.ResponseUserPrincipalSmallVersion;
import org.springframework.http.ResponseEntity;

import javax.validation.Valid;

public class UserServiceImpl implements UserService {
    @Override
    public ResponseEntity<?> authenticateUser(@Valid RequestLogin loginReqeust) {
        return null;
    }

    @Override
    public ResponseEntity<ResponseUserPrincipal> getCurrentUser(String token, StringModel userToken) {
        return null;
    }

    @Override
    public ResponseEntity<ResponseUserPrincipalSmallVersion> getCurrentUserSmallVersion(String email, String authorizationHeader) {
        return null;
    }


    @Override
    public ResponseEntity<ResponseUserPrincipal> registerUser(@Valid RequestSignUp requestSignUp) {
        return null;
    }

    @Override
    public ResponseEntity<ResponseUserPrincipal> updateUser(RequestSignUp requestSignUp) {
        return null;
    }


    @Override
    public ResponseEntity<?> resetPassword(RequestCambioPassword requestCambioPassword) {
        return null;
    }
}
