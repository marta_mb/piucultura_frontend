package it.softlab.piuculturamvc.utility;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

public class CookieUtil {
    //
    // Cookie utility
    //
    public static void setUserCookie(String nameCooke, String userEmail, HttpServletResponse response ){

        if(userEmail != null) {
            // create cookie and set it in model
            Cookie cookie = new Cookie(nameCooke, userEmail);

            cookie.setMaxAge(24 * 60 * 60);

            response.addCookie(cookie);
        }
    }


    //
    // Cookie utility
    //
    public static void invalida (String nameCooke, String userEmail, HttpServletResponse response ){

        // create cookie and set it in model
        Cookie cookie = new Cookie(nameCooke, userEmail);

        cookie.setMaxAge(0);

        response.addCookie(cookie);

    }

}
