package it.softlab.piuculturamvc.entity;


import java.io.Serializable;

public class Group implements Serializable {

    private Long id;

    private GroupName name;

    public Group() {
    }

    public Group(GroupName name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public GroupName getName() {
        return name;
    }

    public void setName(GroupName name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Group{" +
                "id=" + id +
                ", name=" + name +
                '}';
    }
}
