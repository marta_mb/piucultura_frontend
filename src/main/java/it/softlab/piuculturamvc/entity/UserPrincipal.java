package it.softlab.piuculturamvc.entity;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class UserPrincipal {

    private Long id;
    private String name;
    private String surname;
    private Date birth;
    private String username;
    private String country;
    private String profession;
    private Set<Disabilita> disability = new HashSet<>();
    private Set<Interessi> interests = new HashSet<>();
    private String email;
    private String password;
    private Set<Group> groups = new HashSet<>();
    private String qualification;
    private String resetToken;
    private String confirmToken;
    private Boolean confirmEmail;

    public UserPrincipal() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }


    public Set<Disabilita> getDisability() {
        return disability;
    }

    public void setDisability(Set<Disabilita> disability) {
        this.disability = disability;
    }

    public Set<Interessi> getInterests() {
        return interests;
    }

    public void setInterests(Set<Interessi> interests) {
        this.interests = interests;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Group> getGroups() {
        return groups;
    }

    public void setGroups(Set<Group> groups) {
        this.groups = groups;
    }

    public String getResetToken() {
        return resetToken;
    }

    public void setResetToken(String resetToken) {
        this.resetToken = resetToken;
    }

    public String getConfirmToken() {
        return confirmToken;
    }

    public void setConfirmToken(String confirmToken) {
        this.confirmToken = confirmToken;
    }

    public Boolean getConfirmEmail() {
        return confirmEmail;
    }

    public void setConfirmEmail(Boolean confirmEmail) {
        this.confirmEmail = confirmEmail;
    }

    public UserPrincipal(@NotBlank @Size(max = 40) String name, @NotBlank @Size(max = 40) String surname, Date birth, @NotBlank @Size(max = 15) String username, @Size(max = 15) String country, @Size(max = 15) String profession, @Size(max = 15) Set<Disabilita> disability, @Size(max = 15) Set<Interessi> interests, @NotBlank @Size(max = 40) @Email String email, @Size(max = 100) String password, Set<Group> groups, @NotBlank @Size(max = 40) String qualification, @Size(max = 100) String resetToken, @Size(max = 100) String confirmToken, Boolean confirmEmail) {
        this.name = name;
        this.surname = surname;
        this.birth = birth;
        this.username = username;
        this.country = country;
        this.profession = profession;
        this.disability = disability;
        this.interests = interests;
        this.email = email;
        this.password = password;
        this.groups = groups;
        this.qualification = qualification;
        this.resetToken = resetToken;
        this.confirmToken = confirmToken;
        this.confirmEmail = confirmEmail;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    @Override
    public String toString() {
        return "UserPrincipal{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", birth=" + birth +
                ", username='" + username + '\'' +
                ", country='" + country + '\'' +
                ", profession='" + profession + '\'' +
                ", disability=" + disability +
                ", interests=" + interests +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", groups=" + groups +
                ", qualification='" + qualification + '\'' +
                ", resetToken='" + resetToken + '\'' +
                ", confirmToken='" + confirmToken + '\'' +
                ", confirmEmail=" + confirmEmail +
                '}';
    }
}
