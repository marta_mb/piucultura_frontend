package it.softlab.piuculturamvc.entity;


import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;


public class Interessi {

    private Long id;

    private InterestsType name;

    private String altriInteressi;

    public Interessi() {
    }

    public Interessi(InterestsType name) {
        this.name = name;
    }

    public Interessi(InterestsType interestsType, @NotBlank @Size(max = 40) String altriInteressi) {
        this.name = interestsType;
        this.altriInteressi = altriInteressi;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public InterestsType getName() {
        return name;
    }

    public void setName(InterestsType interestsType) {
        this.name = interestsType;
    }

    public String getAltriInteressi() {
        return altriInteressi;
    }

    public void setAltriInteressi(String altriInteressi) {
        this.altriInteressi = altriInteressi;
    }

    @Override
    public String toString() {
        return "Interessi{" +
                "id=" + id +
                ", interestsType=" + name +
                ", altriInteressi='" + altriInteressi + '\'' +
                '}';
    }
}
