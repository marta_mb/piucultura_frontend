package it.softlab.piuculturamvc.entity;

public enum InterestsType {

    ARTE_CONTEMPORANEA,
    SCULTURA,
    ARTE_CLASSICA,
    SITI_CULTURALI,
    MONUMENTI,
    STORIA
}
