package it.softlab.piuculturamvc.entity;

public enum DisabilityName {

    NESSUNA,
    NON_VEDENTE,
    NON_UDENTE,
    PARAPLEGICO
}
