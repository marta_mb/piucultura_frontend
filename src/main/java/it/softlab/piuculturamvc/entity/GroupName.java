package it.softlab.piuculturamvc.entity;

public enum GroupName {

    ROLE_USER,
    ROLE_ADMIN
}
