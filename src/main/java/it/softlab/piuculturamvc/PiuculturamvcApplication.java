package it.softlab.piuculturamvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
//spring.config.location
@SpringBootApplication
@EnableFeignClients
public class PiuculturamvcApplication {


    public static void main(String[] args) {
        SpringApplication.run(PiuculturamvcApplication.class, args);
    }

}

