package it.softlab.piuculturamvc.security;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;


@SuppressWarnings("Duplicates")
public class JWTAuthenticationFilter extends OncePerRequestFilter {
    private static final Logger log = LoggerFactory.getLogger(JWTAuthenticationFilter.class);

    @Override
    public void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        // recupero il token
        String authToken = getToken(request);
        String username = getUsername(request);
        // token trovato provo a validarlo
        if (authToken != null) { //se non c'è token non sei autenticato

            System.out.println("token trovato");
                    //chiamo metodo, se va male             SecurityContextHolder.getContext().setAuthentication(null);
            User user = new User();//todo
            user.setEmail(username);
                // creo oggetto userDto dell'utente connesso
                 //User userDto = new User(username,firstname, lastname, email, nickname, phoneNumber,birthdate, groups);

                // imposto nel security context oggetto utente connesso
                JWTAuthentication authentication = new JWTAuthentication( user );
                authentication.setToken( authToken );
                SecurityContextHolder.getContext().setAuthentication( authentication );



        }
        // token non trovato autenticazione fallita
        else {
            log.error("Autentication error: JWT do not found");
            SecurityContextHolder.getContext().setAuthentication( null );
/*
            SecurityContextHolder.getContext().setAuthentication(null);
*/
        }

        chain.doFilter(request, response);
    }


    /**
     * Recupera dalle header o dai cookies il JWT
     * @param request La HTTPrequest
     * @return String il JWT recuperato
     */
    private String getUsername(HttpServletRequest request) {

        // altirmenti provo a cercarlo nei cookies
         if (request.getCookies()!=null) {
            Cookie[] cookies = request.getCookies();
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals("username_pc")) {//cerca cooki con nome jwt_token
                        return cookie.getValue();
                    }
                }
            }
        }

        return null;
    }

    private String getToken(HttpServletRequest request) {

        // altirmenti provo a cercarlo nei cookies
        if (request.getCookies()!=null) {
            Cookie[] cookies = request.getCookies();
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals("token_pc")) {//cerca cooki con nome jwt_token
                        return cookie.getValue();
                    }
                }
            }
        }

        return null;
    }

}