package it.softlab.piuculturamvc.security;

import org.springframework.security.authentication.AbstractAuthenticationToken;


public class JWTAuthentication extends AbstractAuthenticationToken {

    private String token;
    private User principle;

    public JWTAuthentication(User principle ) {
        super( principle.getAuthorities() );
        this.principle = principle;
    }

    public String getToken() {
        return token;
    }

    public void setToken( String token ) {
        this.token = token;
    }

    @Override
    public boolean isAuthenticated() {
        return true;
    }

    @Override
    public Object getCredentials() {
        return token;
    }

    @Override
    public User getPrincipal() {
        return principle;
    }

}
