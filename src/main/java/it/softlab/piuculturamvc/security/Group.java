package it.softlab.piuculturamvc.security;

import org.springframework.security.core.GrantedAuthority;

public class Group implements GrantedAuthority {
    private String authority;

    public Group(String authority) {
        this.authority = authority;
    }

    @Override
    public String getAuthority() {
        return this.authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }
}
