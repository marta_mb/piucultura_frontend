package it.softlab.piuculturamvc.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

@EnableWebSecurity
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Bean
    public JWTAuthenticationFilter jwtAuthenticationTokenFilter() throws Exception {
        return new JWTAuthenticationFilter();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception{
        http
                .addFilterBefore(jwtAuthenticationTokenFilter(), BasicAuthenticationFilter.class)
                .authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/login").permitAll()
                .antMatchers("/auth/*").permitAll()
                .antMatchers("/dologin").permitAll()
                .antMatchers("/ripristina").permitAll()
                .antMatchers("/forgot").permitAll()
                .antMatchers("/accessdenied").permitAll()
                .antMatchers("/register").permitAll()
                .antMatchers("/js/**").permitAll()
                .antMatchers("/css/**").permitAll()
                .antMatchers("/font-awesome/**").permitAll()
                .antMatchers("/fonts/**").permitAll()
                .antMatchers("/img/**").permitAll()
                .antMatchers("(/error").permitAll()
                .anyRequest().permitAll()
                .and()
                    .formLogin()
                        .loginPage("/login")
                        .successForwardUrl("/")
/*
                        .failureUrl("/login-error.html")
*/

                .and()
                    .csrf()
                .and()
                .logout().logoutUrl("/logout").logoutSuccessUrl("/login").deleteCookies("JSESSIONID").invalidateHttpSession(true)
                .disable();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(HttpMethod.POST, "/login");
        web.ignoring().antMatchers(HttpMethod.POST, "/dologin");
        web.ignoring().antMatchers(HttpMethod.POST, "/register");
        web.ignoring().antMatchers(HttpMethod.POST, "/ripristina");
        web.ignoring().antMatchers(HttpMethod.POST, "/auth/*");

    }


}
