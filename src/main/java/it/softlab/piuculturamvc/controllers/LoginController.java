package it.softlab.piuculturamvc.controllers;

import feign.Param;
import feign.codec.DecodeException;
import it.softlab.piuculturamvc.model.ResponseModel;
import it.softlab.piuculturamvc.model.StringModel;
import it.softlab.piuculturamvc.model.general.UserPrincipalDto;
import it.softlab.piuculturamvc.model.request.*;
import it.softlab.piuculturamvc.model.response.ResponseCambioPassword;
import it.softlab.piuculturamvc.model.response.ResponseJwtAuthentication;
import it.softlab.piuculturamvc.model.response.ResponseUserPrincipal;
import it.softlab.piuculturamvc.security.JWTAuthentication;
import it.softlab.piuculturamvc.security.User;
import it.softlab.piuculturamvc.services.AuthService;
import it.softlab.piuculturamvc.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static it.softlab.piuculturamvc.utility.CookieUtil.invalida;
import static it.softlab.piuculturamvc.utility.CookieUtil.setUserCookie;

@Controller

public class LoginController {
    @Autowired
    AuthService authService;

    @Autowired
    UserService userService;

    private static final Logger log = LoggerFactory.getLogger(LoginController.class);



    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
            invalida("username_pc", "", response);
            invalida("token_pc", "", response);
        }
        return "redirect:/login";//You can redirect wherever you want, but generally it's a good practice to show login screen again.
    }

    @PostMapping("/login")
    public String login(@ModelAttribute("user") RequestSignUp user, Model model, @CookieValue(value = "setUser", defaultValue = "") String setUser,
                           HttpServletResponse response, HttpServletRequest request) {

        boolean isError= false;

        String returnPage = null;
        if(user.getEmail() == null || user.getPassword() == null){
            model.addAttribute("errorMessage","Errore, campi non compilati correttamente");
            isError = true;
        }
        if(user.getEmail() != null && user.getPassword() != null)
            if(user.getEmail().length()==0 || user.getPassword().length()==0){
                model.addAttribute("errorMessage","Errore, campi vuoti");
                isError = true;
            }
        if(isError == false) {
            RequestLogin requestLogin = new RequestLogin();
            requestLogin.setUsernameOrEmail(user.getEmail());
            requestLogin.setPassword(user.getPassword());
            ResponseEntity<ResponseJwtAuthentication> responseEntity = (ResponseEntity<ResponseJwtAuthentication>) authService.authenticateUser(requestLogin);

            /*// Authenticate the user
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            SecurityContext securityContext = SecurityContextHolder.getContext();
            securityContext.setAuthentication(authentication);*/


            if (responseEntity != null && responseEntity.getStatusCodeValue() == 200) {

                if (responseEntity.getBody().getError() == false) {
                    model.addAttribute("resultLoginSucces", "Login avvenuto con successo");
                    setUserCookie("username_pc", user.getEmail(), response);
                    setUserCookie("token_pc", responseEntity.getBody().getResult().getAccessToken(), response);
                    return "redirect:/";

                } else // c'è un errore
                {
                    model.addAttribute("resultLoginError", "Errore, login");
                    return "redirect:/messaggioerrore?error="+responseEntity.getBody().getErrorMessage();

                }
            } else {
                model.addAttribute("resultLoginError", "Errore, login");
                returnPage ="redirect:/messaggioerrore?error="+ "Errore login";


            }
        } else {
            model.addAttribute("resultLoginError", "Errore, login");
            returnPage ="redirect:/messaggioerrore?error="+ "Errore, campi non compilati correttamente";
        }
        if(returnPage != null) return returnPage; else
        return "content/login";
    }









}
