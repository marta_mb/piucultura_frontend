package it.softlab.piuculturamvc.controllers;

import it.softlab.piuculturamvc.model.StringModel;
import it.softlab.piuculturamvc.model.request.RequestCambioPassword;
import it.softlab.piuculturamvc.model.request.RequestResetPassword;
import it.softlab.piuculturamvc.model.response.ResponseCambioPassword;
import it.softlab.piuculturamvc.model.response.ResponseModelString;
import it.softlab.piuculturamvc.security.JWTAuthentication;
import it.softlab.piuculturamvc.services.AuthService;
import it.softlab.piuculturamvc.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static it.softlab.piuculturamvc.utility.CookieUtil.setUserCookie;

@Controller
public class PasswordController {
        @Autowired
        AuthService authService;

        @Autowired
        UserService userService;

        private static final Logger log = LoggerFactory.getLogger(it.softlab.piuculturamvc.controllers.LoginController.class);

    @RequestMapping("/forgot")
    public String forgot(@ModelAttribute("userEmail") StringModel userEmail, Model model, HttpServletResponse response) {
        log.info("Spring Boot Thymeleaf Configuration Example");

        ResponseEntity<ResponseCambioPassword> responseEntity = authService.processForgotPasswordForm(userEmail);

        if (responseEntity != null && responseEntity.getStatusCodeValue() == 200) {

            if (responseEntity.getBody().getError() == false) {
                model.addAttribute("resultInvioEmailSuccess", "Controlla la tua email");
                return "redirect:/conferma";

            } else {
                model.addAttribute("resultInvioEmailSuccess", "Controlla la tua email");
                return "redirect:/conferma";
            }
        } else {
            model.addAttribute("resultInvioEmailError", "Si è verificato un errore");

        }


        return "content/forgotPass";
    }

    @GetMapping("/auth/reset")
    public String getRipristina(@RequestParam(value = "token") String tokenUserResetPassword, @ModelAttribute("richiestaCambioPassword") RequestResetPassword richiestaCambioPassword, Model model,
                                HttpServletResponse response, HttpServletRequest request) {
        setUserCookie("tokenForgotPassword", tokenUserResetPassword, response );

        return "content/ripristinaPassword";
    }

    @PostMapping("/auth/reset")
    public String reset( @ModelAttribute("richiestaResetPassword") RequestResetPassword richiestaResetPassword, Model model,
                         HttpServletResponse response, HttpServletRequest request) {

        boolean isError= false;
        Cookie[] cookies = request.getCookies();

        String tokenUserResetPassword="";
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("tokenForgotPassword")) {
                    tokenUserResetPassword = cookie.getValue();
                }
            }
        }
        String returnPage=null;
        if(richiestaResetPassword.getNewPassword() == null ||tokenUserResetPassword == null) {
            isError = true;
            model.addAttribute("errorMessage", "Errore, dati non validi");
        }
        if(isError == false) {
            RequestResetPassword requestResetPassword = new RequestResetPassword();
            requestResetPassword.setToken(tokenUserResetPassword);
            requestResetPassword.setNewPassword(richiestaResetPassword.getNewPassword());

            ResponseEntity<ResponseCambioPassword> responseEntity = authService.processResetPasswordForm(requestResetPassword);

            if (responseEntity != null && responseEntity.getStatusCodeValue() == 200) {

                if (responseEntity.getBody().getError() == false) {
                    model.addAttribute("resultRipristinoSucces", "Password aggiornata");
                    return "redirect:/login";

                } else // c'è un errore
                {
                    model.addAttribute("resultRipristinoError", "Errore, reset password");
                    returnPage=  "redirect:/messaggioerrore?error="+responseEntity.getBody().getErrorMessage();

                }
            } else {
                model.addAttribute("resultRipristinoError", "Errore, reset password");
                returnPage=  "redirect:/messaggioerrore?error="+responseEntity.getBody().getErrorMessage();

            }
        } else  {
            returnPage =  "redirect:/messaggioerrore?error="+"Errore reset password";

        }

        if(returnPage != null ){
            return returnPage;
        } else
            return "content/ripristinaPassword";
    }

    @GetMapping("/auth/userpassword")
    public String getsceglipassword(@RequestParam(value = "token") String tokenUserResetPassword, @ModelAttribute("richiestaaccount") RequestResetPassword richiestaCambioPassword, Model model,
                                    HttpServletResponse response, HttpServletRequest request) {
        setUserCookie("tokenConfirmEmail", tokenUserResetPassword, response );

        return "content/scegliPassword";
    }

    @PostMapping("/auth/userpassword") //salva la password dopo la registrazione
    public String postScegliPassword( @ModelAttribute("richiestaResetPassword") RequestResetPassword richiestaResetPassword, Model model,
                                      HttpServletResponse response, HttpServletRequest request) {

        boolean isError= false;
        Cookie[] cookies = request.getCookies();

        String tokenUserResetPassword="";
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("tokenConfirmEmail")) {
                    tokenUserResetPassword = cookie.getValue();
                }
            }
        }
        String returnPage=null;
        if(richiestaResetPassword.getNewPassword() == null ||tokenUserResetPassword == null) {
            isError = true;
            model.addAttribute("errorMessage", "Errore, dati non validi");
        }
        if(isError == false) {
            RequestResetPassword requestResetPassword = new RequestResetPassword();
            requestResetPassword.setToken(tokenUserResetPassword);
            requestResetPassword.setNewPassword(richiestaResetPassword.getNewPassword());

            ResponseEntity<ResponseCambioPassword> responseEntity = authService.processSavePsw(requestResetPassword);

            if (responseEntity != null && responseEntity.getStatusCodeValue() == 200) {

                if (responseEntity.getBody().getError() == false) {
                    model.addAttribute("resultRipristinoSucces", "Password aggiornata");
                    return "redirect:/login";

                } else // c'è un errore
                {
                    model.addAttribute("resultRipristinoError", "Errore, reset password");
                    returnPage=  "redirect:/messaggioerrore?error="+responseEntity.getBody().getErrorMessage();

                }
            } else {
                model.addAttribute("resultRipristinoError", "Errore, reset password");
                returnPage=  "redirect:/messaggioerrore?error="+responseEntity.getBody().getErrorMessage();

            }
        } else  {
            returnPage =  "redirect:/messaggioerrore?error="+"Errore reset password";

        }

        if(returnPage != null ){
            return returnPage;
        } else
            return "content/ripristinaPassword";
    }


    @PostMapping("/auth/cambioUserpassword")
    public String cambioPassword(@ModelAttribute("richiestaCambioPassword") RequestCambioPassword richiestaCambioPassword, Model model,
                                 HttpServletResponse response, HttpServletRequest request, @CookieValue(value = "username_pc", defaultValue = "") String username,@CookieValue(value = "token_pc", defaultValue = "") String token) {

        boolean isError= false;

        if(richiestaCambioPassword.getNewPassword() == null || richiestaCambioPassword.getOldPassword() == null) {
            isError = true;
            model.addAttribute("errorMessage", "Errore, dati non validi");
        }
        if(isError == false) {
            RequestCambioPassword requestLogin = new RequestCambioPassword();
            requestLogin.setUsername(username);
            requestLogin.setNewPassword(richiestaCambioPassword.getNewPassword());
            requestLogin.setOldPassword(richiestaCambioPassword.getOldPassword());

            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//            User user = (User)authentication.getPrincipal();
//            user.getUsername();


            ResponseEntity<ResponseCambioPassword> responseEntity = authService.processForgotPasswordForm(token,requestLogin);
            if (responseEntity != null && responseEntity.getStatusCodeValue() == 200) {

                if (responseEntity.getBody().getError() == false) {
                    model.addAttribute("resultRipristinoSucces", "Password cambiata con successo");
                    return "redirect:/";

                } else // c'è un errore
                {
                    model.addAttribute("resultRipristinoError", responseEntity.getBody().getErrorMessage());
                }
            } else {
                model.addAttribute("resultRipristinoError", "Errore");

            }
        }
        return "content/cambioPassword";
    }

    @PostMapping("/auth/reinvia")
    public String reinvia(@ModelAttribute("userEmail") StringModel userEmail, Model model, HttpServletResponse response) {
        log.info("Spring Boot Thymeleaf Configuration Example");

        ResponseEntity<ResponseModelString> responseEntity = authService.reinviaEmailConvermaAccount(userEmail);

        if (responseEntity != null && responseEntity.getStatusCodeValue() == 200) {

            if (responseEntity.getBody().getError() == false) {
                model.addAttribute("resultInvioEmailSuccess", "Controlla la tua email");
                return "redirect:/conferma";

            } else {
                model.addAttribute("resultRipristinoError", "Errore ");
                return   "redirect:/messaggioerrore?error="+ "Account confermato";
            }
        } else {
            model.addAttribute("resultInvioEmailError", "Si è verificato un errore");

        }


        return "content/reinvia";
    }

    @GetMapping("/auth/reinvia")
    public String getreinvia(@ModelAttribute("userEmail") StringModel userEmail, Model model, HttpServletResponse response) {
        log.info("Spring Boot Thymeleaf Configuration Example");

        return "content/reinvia";
    }
}
