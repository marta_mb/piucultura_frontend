package it.softlab.piuculturamvc.controllers;

import com.fasterxml.jackson.databind.BeanProperty;
import feign.codec.DecodeException;
import it.softlab.piuculturamvc.model.StringModel;
import it.softlab.piuculturamvc.model.general.UserPrincipalDto;
import it.softlab.piuculturamvc.model.general.UserPrincipalDtoSmalVersion;
import it.softlab.piuculturamvc.model.request.RequestSignUp;
import it.softlab.piuculturamvc.model.response.ResponseUserPrincipal;
import it.softlab.piuculturamvc.model.response.ResponseUserPrincipalSmallVersion;
import it.softlab.piuculturamvc.services.AuthService;
import it.softlab.piuculturamvc.services.UserService;
import org.bouncycastle.cert.ocsp.Req;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class ProfiloController {
    @Autowired
    AuthService authService;

    @Autowired
    UserService userService;

    private static final Logger log = LoggerFactory.getLogger(LoginController.class);

    @GetMapping("/profilo")
    public String getProfilo(@CookieValue(value = "username_pc", defaultValue = "") String setUser, @ModelAttribute("responseUserPrincipal") RequestSignUp responseUserPrincipal, Model model,
                             HttpServletResponse response, HttpServletRequest request,@CookieValue(value = "token_pc", defaultValue = "") String token) {


        if(setUser != null && setUser.length()>0) {
          //  try {
            StringModel stringModel = new StringModel();
            stringModel.setValue(setUser);
                ResponseEntity<ResponseUserPrincipal> currentUser = userService.getCurrentUser(token, stringModel);

                if(currentUser.getBody().getResult().getName() != null)
                model.addAttribute("name", currentUser.getBody().getResult().getName());
                if(currentUser.getBody().getResult().getSurname() != null)
                model.addAttribute("surname", currentUser.getBody().getResult().getSurname());
            if(currentUser.getBody().getResult().getEmail() != null)
                model.addAttribute("email", currentUser.getBody().getResult().getEmail());
            if(currentUser.getBody().getResult().getUsername() != null)
                model.addAttribute("username", currentUser.getBody().getResult().getUsername());
            if(currentUser.getBody().getResult().getBirth() != null)
                model.addAttribute("birth", currentUser.getBody().getResult().getBirth());
            if(currentUser.getBody().getResult().getCountry() != null)
                model.addAttribute("country", currentUser.getBody().getResult().getCountry());
            if(currentUser.getBody().getResult().getName() != null)
                model.addAttribute("qualification", currentUser.getBody().getResult().getQualification());
            if(currentUser.getBody().getResult().getProfession() != null)
                model.addAttribute("profession", currentUser.getBody().getResult().getProfession());
            if(currentUser.getBody().getResult().getProfession() != null)
                model.addAttribute("interests", currentUser.getBody().getResult().getProfession());
        }
        return "content/signup";
    }


    @PostMapping("/profilo")
    public String saveProfilo(@CookieValue(value = "username_pc", defaultValue = "") String setUser, @ModelAttribute("responseUserPrincipal") RequestSignUp responseUserPrincipal, Model model,
                              HttpServletResponse response, HttpServletRequest request, @CookieValue(value = "username_pc", defaultValue = "") String username,@CookieValue(value = "token_pc", defaultValue = "") String token) {

        String successMessage = "Account creato con successo";
        String errorMessage = "Creazione account -> errore";
        String returnPage = null;
        Boolean isUpdate = false;
        if(setUser != null && setUser.length()>0 ){
            isUpdate = true;
            errorMessage = "Aggiornamento profilo -> Errore";
            successMessage = "Aggiornamento avvenuto con successo";
        }


        RequestSignUp requestSignUp = new RequestSignUp();
        BeanUtils.copyProperties(responseUserPrincipal,requestSignUp);

        try {
            ResponseEntity<ResponseUserPrincipal> responseEntity = null;
            if(isUpdate) {
                StringModel stringModel = new StringModel();
                stringModel.setValue(token);
                ResponseEntity<ResponseUserPrincipal> currentUser = userService.getCurrentUser(token, stringModel);
                UserPrincipalDto result = currentUser.getBody().getResult();
                Long id = result.getId();
                String password = result.getPassword();
                BeanUtils.copyProperties(requestSignUp ,result);
                RequestSignUp requestSignUp1 = new RequestSignUp();
                BeanUtils.copyProperties( result, requestSignUp1);
                requestSignUp1.setId(id);
                requestSignUp1.setPassword(password);
                responseEntity = userService.registerUser(requestSignUp1);
            } else {
                responseEntity = userService.registerUser(requestSignUp);
            }
            if (responseEntity != null && responseEntity.getStatusCodeValue() == 200) {

                if (responseEntity.getBody().getError() == false) {

                    model.addAttribute("resultLoginSucces", successMessage);
                    if(!isUpdate) {
                        return "redirect:/conferma";

                    }
                    return "redirect:/";

                } else // c'è un errore
                {
                    model.addAttribute("resultLoginError", errorMessage);
                    return "redirect:/messaggioerrore?error="+responseEntity.getBody().getErrorMessage();
                }
            } else {
                model.addAttribute("resultLoginError", errorMessage);
                return "redirect:/messaggioerrore?error="+ "Errore registrazione";

            }

        } catch (DecodeException d){
            log.error(d.getMessage());
        }
        if(!isUpdate) {
            return "redirect:/conferma";

        }if(returnPage!=null) return returnPage; else
            return "content/signup";

    }

















/*    @PostMapping("/profilo")
    public String saveProfilo(@CookieValue(value = "username_pc", defaultValue = "") String setUser, @ModelAttribute("responseUserPrincipal") UserPrincipalDto responseUserPrincipal, Model model,
                              HttpServletResponse response, HttpServletRequest request, @CookieValue(value = "username_pc", defaultValue = "") String username) {

        String successMessage = "Account creato con successo";
        String errorMessage = "Creazione account -> errore";
        String returnPage = null;
        Boolean isUpdate = false;
        if(setUser != null && setUser.length()>0 ){
            isUpdate = true;
            errorMessage = "Aggiornamento profilo -> Errore";
            successMessage = "Aggiornamento avvenuto con successo";
        }

        RequestSignUp requestSignUp = new RequestSignUp();
        requestSignUp.setName(responseUserPrincipal.getName());
        requestSignUp.setUsername(responseUserPrincipal.getUsername());
        requestSignUp.setEmail(responseUserPrincipal.getEmail());

        try {
            ResponseEntity<ResponseUserPrincipal> responseEntity = null;
            if(isUpdate) {
                responseEntity = userService.updateUser(requestSignUp);
            } else {
                responseEntity = userService.registerUser(requestSignUp);
            }
            if (responseEntity != null && responseEntity.getStatusCodeValue() == 200) {

                if (responseEntity.getBody().getError() == false) {

                    model.addAttribute("resultLoginSucces", successMessage);
                    if(!isUpdate) {
                        return "redirect:/conferma";

                    }
                    return "redirect:/";

                } else // c'è un errore
                {
                    model.addAttribute("resultLoginError", errorMessage);
                    returnPage= "redirect:/messaggioerrore?error="+responseEntity.getBody().getErrorMessage();
                }
            } else {
                model.addAttribute("resultLoginError", errorMessage);
                returnPage = "redirect:/messaggioerrore?error="+ "Errore registrazione";

            }

        } catch (DecodeException d){
            log.error(d.getMessage());
        }
        if(!isUpdate) {
            return "redirect:/conferma";

        }if(returnPage!=null) return returnPage; else
            return "content/profilo";
    }*/
}
