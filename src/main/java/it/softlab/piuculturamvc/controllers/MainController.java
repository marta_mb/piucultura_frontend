package it.softlab.piuculturamvc.controllers;


import it.softlab.piuculturamvc.model.general.UserPrincipalDto;
import it.softlab.piuculturamvc.model.request.RequestLogin;
import it.softlab.piuculturamvc.services.AuthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class MainController {
    private static final Logger log = LoggerFactory.getLogger(it.softlab.piuculturamvc.controllers.LoginController.class);

    @Autowired
    AuthService authService;


    @GetMapping("/")
    public String getHome() {
        return "index";
    }

 @GetMapping("/accessdenied")
 public String accessDenied() {
     System.out.println("accessDenied");

     return "/error/access-denied";
 }

  /*  *//*add userDto in model attribute*//*
    @ModelAttribute("user")
    public UserPrincipalDto setUpUserForm() {
        return new UserPrincipalDto();
    }*/

    @GetMapping("/forgot")
    public String forgotPassword() {
        return "/content/forgotPass.html";
    }

    @RequestMapping("/login")
    public String login(Model model) {
        log.info("Spring Boot Thymeleaf Configuration Example");
        return "content/login";
    }
    @GetMapping("/conferma")
    public String confermaAccount( @CookieValue(value = "username_pc", defaultValue = "") String setUser) {

        return "content/confirm";
    }

    @GetMapping("/messaggioerrore")
    public String messaggioerrore(@CookieValue(value = "username_pc", defaultValue = "") String setUser, @RequestParam(value = "error") String errorMessage, Model model) {
        model.addAttribute("errorMessage", errorMessage);
        return "content/errorMessage";
    }
    @GetMapping("/cambiopassword")
    public String getCambiaPassword( @CookieValue(value = "username_pc", defaultValue = "") String setUser) {

        return "content/cambioPassword";
    }



}
